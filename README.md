# hackaBLE_zephyr

Using Zephyr RTOS with Electronut Labs Nordic nRF52832 hackaBLE.

## Wiki Article

https://electronut.in/getting-started-with-zephyr-rtos-on-nordic-nrf52832-hackable/


## Notes

 cmake -GNinja -DBOARD=nrf52_hackable ..
 ninja flash

picocom -e b -b 115200 /dev/ttyACM1
